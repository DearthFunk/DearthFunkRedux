module.exports = function() {
  return {
    'free-solid-svg-icons': 'all',
    'free-brands-svg-icons': [
      'github',
      'stack-overflow'
    ]
  };
};