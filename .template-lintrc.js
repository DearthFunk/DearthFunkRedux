'use strict';

module.exports = {
  extends: 'octane',

  //https://github.com/ember-template-lint/ember-template-lint#rules
  rules: {
    'no-bare-strings': false, //todo: localize
  }

};
