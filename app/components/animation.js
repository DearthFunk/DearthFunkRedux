import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { CanvasState } from '../animations/canvas-state';

export default class AnimationComponent extends Component {

  @tracked state = new CanvasState();

   /////////////////////////////////////////////////////////// 

  @action
  animate() {
    if (this.ctx.canvas.width > 0 && this.ctx.canvas.height > 0) {
      this.args.model?.draw(this.ctx, this.state);
    }
    requestAnimationFrame(this.animate); //THE LOOP
  }

  @action
  canvasInsertEvent(elem) {
    this.canvas = elem;
    this.ctx = this.canvas.getContext('2d');
    requestAnimationFrame(this.animate); //THE BIG KICK-OFF
  }

  @action
  canvasResizeEvent(event) {
    if (!event) { return; }
    this.state.w = event.contentRect.width;
    this.state.h = event.contentRect.height;
  }

  handleMoveEvent(event) {
    if (!event) { return; }
    this.state.x = event.clientX;
    this.state.y = event.clientY;
  }

  handleButtonEvent(event) {
    if (!event) { return; }
    this.state.mouseDown = event.buttons === 1;
  }
}