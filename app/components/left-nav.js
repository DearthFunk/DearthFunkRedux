import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import anim from '../animations/animations';

const Link = (_id, name, description) => {
    //3 props which cascade through order of key definitions
    //todo: is there an es6 way of doing this shorthand?
    return {
        _id,
        name: name || _id,
        description: description || name || _id
    }
}

export default class LeftNavComponent extends Component {

    @tracked anim = anim;
    @tracked apps = [
        Link('googolgears', 'go gol gears', '10 to the power of 100'),
        Link('djangular'),
        Link('animations'),
        Link('cellularautomation'),
        Link('hexsynth'),
        Link('discbeats'),
        Link('discsynth'),
        Link('ideaboard'),
        Link('gencolors')
    ]
    @tracked albums = [
        Link('tmotrd'),
        Link('oot'),
        Link('methanger'),
        Link('eviltech'),
        Link('psetmyptw')
    ]
}
