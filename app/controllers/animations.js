import Controller from '@ember/controller';

export default class AnimationsController extends Controller {
  queryParams = ['name'];
  name = null;
}
