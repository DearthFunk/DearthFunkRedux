import Route from '@ember/routing/route';
import anim from '../animations/animations';

export default class AnimationsRoute extends Route {

  queryParams = {
    name: { refreshModel: true }
  }

  model(params) {
    return anim.getAnimation(params.name);
  }
}
