import Bubbles from './items/bubbles';
import LineConnections from './items/line-connections';
import ParticleRing from './items/particle-ring';
import Ring from './items/ring';
import Squares from './items/squares';
import Stopper from './items/stopper';
import Tracer from './items/tracer';
import WhirlyParticles from './items/whirly-particles';

let animations = [
  new Bubbles('bubbles', '.bubble?splosion', 'click for new color set; mouse center to decay'),
  new LineConnections('line_connection', 'Line Connections', 'hold mouse down and/or move mouse around red dots'),
  new ParticleRing('particle_ring'),
  new Ring('ring', 'dual ring', 'mouse center to decay and mouse discovery'),
  new Squares('squares'),
  new Stopper('stopper'),
  new Tracer('tracer'),
  new WhirlyParticles('whirly_particels')
];

let getAnimation = (_id) => {
  return animations.find(item => item._id === _id);
}

export { animations, getAnimation};