const COLOR_TYPE = {
  HEX: 0,
  RGB: 1,
  RGBA: 2,
  HSL: 3,
  HSLA: 4
};

const RETURN_TYPE = {
  STRING: 0,
  ARRAY: 1,
  OBJECT: 2
};

const getColorType = (color) => {
  const c = color.slice(0, 4).toLowerCase();
  return (color[0] === '#' ? COLOR_TYPE.HEX :
      c === 'rgb(' ? COLOR_TYPE.RGB :
        c === 'rgba' ? COLOR_TYPE.RGBA :
          c === 'hsl(' ? COLOR_TYPE.HSL :
            c === 'hsla' ? COLOR_TYPE.HSLA :
              false
  );
};

const getValues = (color, returnType, hslPercentage) => {
  color = color.toUpperCase();
  const obj = {};
  let returnVal, vals, keys;
  const colorType = generate.colorType(color);
  if (colorType === COLOR_TYPE.HEX) {
    const arr = color.slice(1).split('').slice(0, 3);
    vals = color.length < 7 ?
      [arr[0] + arr[0], arr[1] + arr[1], arr[2] + arr[2]] :
      color.slice(1).match(/.{1,2}/g);
    keys = ['r', 'g', 'b'];
  } else {
    vals = color.replace(/([(])/g, ',').replace(/([)])/g, '').split(',');
    keys = vals[0].toLowerCase().split('');
    vals.splice(0, 1);
  }
  for (let i = 0; i < keys.length; i++) {
    if (colorType === COLOR_TYPE.HEX) {
      obj[keys[i]] = vals[i];
    } else if ( vals[i].indexOf('%') > -1) {
      if (hslPercentage) {
        vals[i] = Number(vals[i].replace('%', '')) / 100;
      }
      obj[keys[i]] = vals[i];
    } else {
      obj[keys[i]] =  Number(vals[i]);
      vals[i] = Number(vals[i]);
    }
  }
  switch (returnType) {
    case RETURN_TYPE.STRING: returnVal = color; break;
    case RETURN_TYPE.ARRAY: returnVal = vals; break;
    case RETURN_TYPE.OBJECT: returnVal = Object.create(obj); break;
    default: returnVal = color;
  }
  return returnVal;
}

const getRandomNumber = (from, to, decimals) => {
  return Number((Math.random() * (Number(to) - Number(from)) + Number(from)).toFixed(decimals));
}

const getRoundedNumber = (value, decimals) => {
  const precision = decimals || 0;
  const neg = value < 0;
  const power = Math.pow(10, precision);
  const newvalue = Math.round(value * power);
  const integral = String((neg ? Math.ceil : Math.floor)(newvalue / power));
  const fraction = String((neg ? -newvalue : newvalue) % power);
  const padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');
  return parseFloat(precision ? integral + '.' +  padding + fraction : integral);
}

const getHue = (m1, m2, hue, numOrHex) => {
  let v;
  if (hue < 0) {
    hue += 1;
  } else if (hue > 1) {
    hue -= 1;
  }
  if (6 * hue < 1) {
    v = m1 + (m2 - m1) * hue * 6;
  } else if (2 * hue < 1) {
    v = m2;
  } else if (3 * hue < 2) {
    v = m1 + (m2 - m1) * (2 / 3 - hue) * 6;
  } else {
    v = m1;
  }
  return numOrHex ? convert.numberToHex(255 * v) : generate.roundedNumber(255 * v, 0);
}

const randomHex = (type, greyScale) => {
  let returnVal;
  if (greyScale) {
    const hex =
      generate.randomNumber(0, 15, 0).toString(16).toUpperCase() +
      generate.randomNumber(0, 15, 0).toString(16).toUpperCase();
    returnVal = '#' + hex + hex + hex;
  } else {
    return '#' + (function co(lor){
      return (lor +=
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f'][Math.floor(Math.random() * 16)])
        && (lor.length === 6) ?  lor : co(lor); })('');
  }
  return generate.values(returnVal, type);
}

const randomRgb = (type, greyScale) => {
  const r = generate.randomNumber(0, 255, 0);
  const g = greyScale ? r : generate.randomNumber(0, 255, 0);
  const b = greyScale ? r : generate.randomNumber(0, 255, 0);
  return generate.values('rgb(' + r + ',' + b + ',' + g + ')', type);
}

const randomHsl = (type, greyScale) => {
  const h = generate.randomNumber(0, 240, 0);
  const s = greyScale ? '0%' : generate.randomNumber(0, 100, 0) + '%';
  const l = generate.randomNumber(0, 100, 0) + '%';
  return generate.values('HSL(' + h + ',' + s + ',' + l + ')', type);
}

const randomRgba = (type, greyScale, opacity) => {
  const r =  generate.randomNumber(0, 255, 0);
  const g = greyScale ? r : generate.randomNumber(0, 255, 0);
  const b = greyScale ? r : generate.randomNumber(0, 255, 0);
  const a = opacity == null ? generate.randomNumber(0, 1, 4) : opacity;
  return generate.values('RGBA(' + r + ',' + g + ',' + b + ',' + a + ')', type);
}

const randomHsla = (type, greyScale, opacity) => {
  const h = generate.randomNumber(0, 240, 0);
  const s = greyScale ? '0%' : generate.randomNumber(0, 100, 0) + '%';
  const l = generate.randomNumber(0, 100, 0) + '%';
  const a = opacity == null ? generate.randomNumber(0, 1, 4) : opacity;
  return generate.values('HSLA(' + h + ',' + s + ',' + l + ',' + a + ')', type);
}

const convertNumberToHex = (x) => {
  return x >= 0 && x <= 255 ?
    ('0' + parseInt(x, 10).toString(16)).slice(-2).toUpperCase() :
    '00';
}

const convertHexToNumber = (x) => {
  const tmp = x.replace(/[^a-f,0-9]/ig, '');
  return tmp === '' ? 0 : parseInt(tmp, 16);
}

const convertHex = (color) => {
  color = color.toUpperCase();
  let returnVal;
  const colorType = generate.colorType(color);
  const vals = generate.values(color, RETURN_TYPE.ARRAY, true);
  if (colorType === COLOR_TYPE.HEX) {
    if (color.length === 7) {
      returnVal = color;
    } else if (color.length === 4) {
      const tmp = color.split('');
      returnVal = '#' + tmp[1] + tmp[1] + tmp[2] + tmp[2] + tmp[3] + tmp[3];
    } else {
      returnVal = color;
    }
  } else if (colorType === COLOR_TYPE.RGB || colorType === COLOR_TYPE.RGBA) {
    returnVal = '#' +
      convert.numberToHex(vals[0]) +
      convert.numberToHex(vals[1]) +
      convert.numberToHex(vals[2]);
  } else if (colorType === COLOR_TYPE.HSL || colorType === COLOR_TYPE.HSLA) {
    let m1, m2, hue;
    if (vals[1] === 0) {
      vals[0] = vals[1] = vals[2] = (vals[2] * 255);
    } else {
      m2 = vals[2] <= 0.5 ? vals[2] * (vals[1] + 1) : vals[2] + vals[1] - vals[2] * vals[1];
      m1 = vals[2] * 2 - m2;
      hue = vals[0] / 360;
    }
    returnVal = '#' +
      generate.hue(m1, m2, hue + 1 / 3, true) +
      generate.hue(m1, m2, hue, true) +
      generate.hue(m1, m2, hue - 1 / 3, true);
  } else {
    returnVal = color;
  }
  return returnVal;
}

const convertRgb = (color) => {
  color = color.toUpperCase();
  let returnVal;
  const colorType = generate.colorType(color);
  const vals = generate.values(color, RETURN_TYPE.ARRAY, true);

  if (colorType === COLOR_TYPE.HEX) {
    returnVal = 'RGB(' +
      convert.hexToNumber(vals[0]) + ',' +
      convert.hexToNumber(vals[1]) + ',' +
      convert.hexToNumber(vals[2]) + ')';
  } else if (colorType === COLOR_TYPE.RGB) {
    returnVal = color;
  } else if (colorType === COLOR_TYPE.RGBA) {
    returnVal = 'RGB(' + vals[0] + ',' + vals[1] + ',' + vals[2] + ')';
  } else if (colorType === COLOR_TYPE.HSL || colorType === COLOR_TYPE.HSLA) {
    let m1, m2, hue;
    if (vals[1] === 0) {vals[0] = vals[1] = vals[2] = (vals[2] * 255);
    } else {
      m2 = vals[2] <= 0.5 ? vals[2] * (vals[1] + 1) : vals[2] + vals[1] - vals[2] * vals[1];
      m1 = vals[2] * 2 - m2;
      hue = vals[0] / 360;
    }
    returnVal = 'RGB(' +
      generate.hue(m1, m2, hue + 1 / 3) + ',' +
      generate.hue(m1, m2, hue) + ',' +
      generate.hue(m1, m2, hue - 1 / 3) + ')';
  }
  return returnVal;
}

const convertRgba = (color, opacity) => {
  color = color.toUpperCase();
  let returnVal;
  const colorType = generate.colorType(color);
  const vals = generate.values(color, RETURN_TYPE.ARRAY, true);
  const o = colorType === COLOR_TYPE.RGBA ||
    colorType === COLOR_TYPE.HSLA ?
      vals[3] :
      opacity < 0 ? 0 : opacity > 1 || opacity == null ? 1 : opacity;
  if (colorType === COLOR_TYPE.HEX) {
    returnVal = 'RGBA(' +
      convert.hexToNumber(vals[0]) + ',' +
      convert.hexToNumber(vals[1]) + ',' +
      convert.hexToNumber(vals[2]) + ',' +
      o + ')';
  } else if (colorType === COLOR_TYPE.RGB) {
    returnVal = 'RGBA(' + vals[0] + ',' + vals[1] + ',' + vals[2] + ',' + o + ')';
  } else if (colorType === COLOR_TYPE.RGBA) {
    returnVal = color;
  } else if (colorType === COLOR_TYPE.HSL || colorType === COLOR_TYPE.HSLA) {
    let m1, m2, hue;
    if (vals[1] === 0) {
      vals[0] = vals[1] = vals[2] = (vals[2] * 255);
    } else {
      m2 = vals[2] <= 0.5 ? vals[2] * (vals[1] + 1) : vals[2] + vals[1] - vals[2] * vals[1];
      m1 = vals[2] * 2 - m2;
      hue = vals[0] / 360;
    }
    returnVal = 'RGBA(' +
      generate.hue(m1, m2, hue + 1 / 3) + ',' +
      generate.hue(m1, m2, hue) + ',' +
      generate.hue(m1, m2, hue - 1 / 3) + ',' + o + ')';
  }
  return returnVal;
}

const convertHsl = (color) => {
  let returnVal;
  const colorType = generate.colorType(color);
  if (!colorType) { return color; }
  const vals = generate.values(color, RETURN_TYPE.ARRAY, true);
  if ([COLOR_TYPE.HEX, COLOR_TYPE.RGB, COLOR_TYPE.RGBA].indexOf(colorType) !== -1) {
    const hexVals = generate.values(convert.rgb(color), RETURN_TYPE.ARRAY);
    const r = (colorType === COLOR_TYPE.HEX ? hexVals[0] : vals[0]) / 255;
    const g = (colorType === COLOR_TYPE.HEX ? hexVals[1] : vals[1]) / 255;
    const b = (colorType === COLOR_TYPE.HEX ? hexVals[2] : vals[2]) / 255;
    const max = Math.max(r, g, b), min = Math.min(r, g, b);
    let h, s;
    const l = (max + min) / 2;
    if (max === min) {
      h = s = 0; // achromatic
    } else {
      const d = max - min;
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
      switch (max) {
        case r: h = (g - b) / d + (g < b ? 6 : 0); break;
        case g: h = (b - r) / d + 2; break;
        case b: h = (r - g) / d + 4; break;
      }
      h /= 6;
    }
    returnVal = 'HSL(' + Math.floor(h * 360) + ',' + Math.floor(s * 100) + '%,' + Math.floor(l * 100) + '%)';
  } else if (colorType === COLOR_TYPE.HSL) {
    returnVal = color.toUpperCase();
  } else if (colorType === COLOR_TYPE.HSLA) {
    returnVal = 'HSL(' + vals[0] + ',' + (vals[1] * 100) + '%,' + (vals[2] * 100) + '%)';
  }
  return returnVal;
}

const convertHsla = (color, opacity) => {
  let returnVal;
  const colorType = generate.colorType(color);
  const vals = generate.values(color, RETURN_TYPE.ARRAY, true);
  const o = colorType === COLOR_TYPE.RGBA ||
    colorType === COLOR_TYPE.HSLA ?
      vals[3] :
      opacity < 0 ?
        0 :
        opacity > 1 ||
          opacity ?
            1 :
            opacity;
  if (colorType === COLOR_TYPE.HEX ||
    colorType === COLOR_TYPE.RGB ||
    colorType === COLOR_TYPE.RGBA) {
    const hexVals = generate.values(convert.rgb(color), RETURN_TYPE.ARRAY);
    const r = (colorType === COLOR_TYPE.HEX ? hexVals[0] : vals[0]) / 255;
    const g = (colorType === COLOR_TYPE.HEX ? hexVals[1] : vals[1]) / 255;
    const b = (colorType === COLOR_TYPE.HEX ? hexVals[2] : vals[2]) / 255;
    const max = Math.max(r, g, b), min = Math.min(r, g, b);
    let h, s;
    const l = (max + min) / 2;
    if (max === min) { // achromatic
      h = s = 0;
    } else {
      const d = max - min;
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
      switch (max) {
        case r: h = (g - b) / d + (g < b ? 6 : 0); break;
        case g: h = (b - r) / d + 2; break;
        case b: h = (r - g) / d + 4; break;
      }
      h /= 6;
    }
    returnVal = 'HSLA(' +
      Math.floor(h * 360) + ',' +
      Math.floor(s * 100) + '%,' +
      Math.floor(l * 100) + '%,' +
      o + ')';
  } else if (colorType === COLOR_TYPE.HSL) {
    returnVal = 'HSLA(' + vals[0] + ',' + (vals[1] * 100) + '%,' + (vals[2] * 100) + '%,' + o + ')';
  } else if (colorType === COLOR_TYPE.HSLA) {
    returnVal = color.toUpperCase();
  }
  return returnVal;
}

const arrayHex = (c1, c2, len) => {
  const steps = [];
  let returnVal = [];
  if (len < 3) {
    returnVal = [convert.hex(c1), convert.hex(c2)];
  } else {
    const c1Vals = generate.values(convert.rgb(c1), RETURN_TYPE.ARRAY);
    const c2Vals = generate.values(convert.rgb(c2), RETURN_TYPE.ARRAY);
    for (let i = 0; i < c1Vals.length; i++) {
      steps.push( c1Vals[i] === c2Vals[i] ? 0 : (c1Vals[i] - c2Vals[i]) / (len - 1) * -1 );
    }
    for (let i = 0; i < len; i++) {
      returnVal.push ( '#' +
        convert.numberToHex(Math.floor( steps[0] * i + c1Vals[0] )) +
        convert.numberToHex(Math.floor( steps[1] * i + c1Vals[1] )) +
        convert.numberToHex(Math.floor( steps[2] * i + c1Vals[2] ))
      );
    }
  }
  return returnVal;
}

const arrayRgb = (c1, c2, len) => {
  const steps = [];
  let returnVal = [];
  if (len < 3) {
    returnVal = [convert.rgb(c1), convert.rgb(c2)];
  } else {
    const c1Vals = generate.values(convert.rgb(c1), RETURN_TYPE.ARRAY);
    const c2Vals = generate.values(convert.rgb(c2), RETURN_TYPE.ARRAY);
    for (let i = 0; i < c1Vals.length; i++) {
      steps.push( c1Vals[i] === c2Vals[i] ? 0 : (c1Vals[i] - c2Vals[i]) / (len - 1) * -1 );
    }
    for (let i = 0; i < len; i++) {
      returnVal.push ( 'RGB(' +
        Math.floor( steps[0] * i + c1Vals[0] ) + ',' +
        Math.floor( steps[1] * i + c1Vals[1] ) + ',' +
        Math.floor( steps[2] * i + c1Vals[2] ) + ')'
      );
    }
  }
  return returnVal;
}

const arrayRgba = (c1, c2, len, op1, op2) => {
  const steps = [];
  let returnVal = [];
  if (len < 3) {
    returnVal = [convert.rgba(c1, op1), convert.hex(c2)];
  } else {
    const c1Vals = generate.values(convert.rgba(c1, op1), RETURN_TYPE.ARRAY);
    const c2Vals = generate.values(convert.rgba(c2, op2), RETURN_TYPE.ARRAY);

    for (let i = 0; i < c1Vals.length; i++) {
      steps.push( c1Vals[i] === c2Vals[i] ? 0 : (c1Vals[i] - c2Vals[i]) / (len - 1) * -1 );
    }
    for (let i = 0; i < len; i++) {
      returnVal.push ( 'RGBA(' +
        Math.floor( steps[0] * i + c1Vals[0] ) + ',' +
        Math.floor( steps[1] * i + c1Vals[1] ) + ',' +
        Math.floor( steps[2] * i + c1Vals[2] ) + ',' +
        generate.roundedNumber(Number(steps[3] * i + c1Vals[3]), 2) + ')'
      );
    }
  }
  return returnVal;
}

const arrayHsl = (c1, c2, len) => {
  let returnVal = [];
  if (len < 3) {
    returnVal = [convert.hsl(c1), convert.hsl(c2)];
  } else {
    returnVal = array.rgb(c1, c2, len);
    for (let i = 0; i < returnVal.length; i++) {
      returnVal[i] = convert.hsl(returnVal[i]);
    }
  }
  return returnVal;
}

const arrayHsla = (c1, c2, len, o1, o2) => {
  let returnVal = [];
  if (len < 3) {
    returnVal = [convert.hsla(c1), convert.hsla(c2)];
  } else {
    returnVal = array.rgba(c1, c2, len, o1, o2);
    for (let i = 0; i < returnVal.length; i++) {
      returnVal[i] = convert.hsla(returnVal[i]);
    }
  }
  return returnVal;
}

let generate = {
  colorType: getColorType,
  values: getValues,
  randomNumber: getRandomNumber,
  roundedNumber: getRoundedNumber,
  hue: getHue
};

let random = {
  hex: randomHex,
  rgb: randomRgb,
  rgba: randomRgba,
  hsl: randomHsl,
  hsla: randomHsla
};

let convert = {
  numberToHex: convertNumberToHex,
  hexToNumber: convertHexToNumber,
  hex: convertHex,
  rgb: convertRgb,
  rgba: convertRgba,
  hsl: convertHsl,
  hsla: convertHsla
};

let array = {
  hex: arrayHex,
  rgb: arrayRgb,
  rgba: arrayRgba,
  hsl: arrayHsl,
  hsla: arrayHsla
};

 let getDistance = (x1, y1, x2, y2) => {
  return Math.floor(Math.sqrt(
    Math.pow(x1 - x2, 2) +
    Math.pow(y1 - y2, 2)
  ));
}

export { generate, random, convert, array, getDistance };