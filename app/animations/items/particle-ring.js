export default class ParticleRing extends Animation {
	constructor() {
		super(...arguments);
		this.sprouts = [];
		this.x = 300;
		this.y = 300;
		this.dd = 250;
		this.sprouts.push(
			new Sprout(this.x - this.dd, this.y - this.dd),
			new Sprout(this.x + this.dd, this.y - this.dd),
			new Sprout(this.x - this.dd, this.y + this.dd),
			new Sprout(this.x + this.dd, this.y + this.dd),
			new Sprout(this.x, this.y, 350, '#0000FF', 4, 20, true)
		);
	}
	draw(ctx, state) {
		ctx.globalCompositeOperation = 'soft-light';
		ctx.clearRect(0,0,state.w, state.h);
		this.sprouts.forEach((sprout, index) => {
			if (index === this.sprouts.length -1) {
				ctx.globalCompositeOperation = 'multiply';
			}
			sprout.draw(ctx, state, this._genColors);
		});
	}
}

import Animation from './animation';

class Sprout {
	constructor(x = 100, y = 100, radius = 300, color = '#FF0000', reflections = 20, levels = 30, center = false) {
		this.color = color;
		this.radius = radius;
		this.reflections = reflections;
		this.levels = levels + 1; //adjust for center one
		this._speedTracker = 0;
		this.speed = 0.02;
		this.x = x;
		this.y = y;
		this.center = center;
	}	

	_getXRotator(angle) {
		return Math.cos(angle) * this.radius + this.x; 
	}
	_getYRotator(angle) {
		return Math.sin(angle) * this.radius + this.y;
	}

	draw(ctx, state, genColors) {
		this._speedTracker += this.speed;
		ctx.lineWidth = this.center ? 2: 1;
		let circleHoverAngle = Math.atan2(state.y - this.y, state.x - this.x);
		let rotorX = this._getXRotator(circleHoverAngle);
		let rotorY = this._getYRotator(circleHoverAngle);
		for (let reflection = 0; reflection < this.reflections; reflection++) {
			let angle = reflection/this.reflections * Math.PI  * 2 - (Math.PI/2);
			for (let level = 0; level < this.levels; level++) {
				let r = level / this.levels * this.radius;
				let reverse = (level % 2 == 0 ? 1 : -1);
				let adjustedAngle = angle + (this._speedTracker * reverse);
				let x = this.x + Math.cos(adjustedAngle) * r;
				let y = this.y + Math.sin(adjustedAngle) * r;
				let d = Math.sqrt(Math.pow(rotorX - x, 2) +	Math.pow(rotorY - y, 2));
				let distancefromCenter = genColors.getDistance(this.x, this.y, state.x, state.y);
				let radius = (this.radius/2) - (d/4) * (distancefromCenter/this.radius); 
				if (radius > 0) {
					ctx.beginPath();
					ctx.arc(x,y, radius, 0 , Math.PI*2, false);
					if (level ==0){
						ctx.fillStyle = genColors.convert.rgba(this.color,0.1);
						ctx.fill();
					}
					ctx.strokeStyle = genColors.convert.rgba('#000000',1-level/this.levels);
					ctx.stroke();
					ctx.closePath();
				}
			}
		}
	}
}
