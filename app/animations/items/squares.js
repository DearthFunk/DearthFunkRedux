import Animation from './animation';

export default class Squares extends Animation{

	rotations = [true, false, false, false, true, false, false, false];
	rotationsText = ['X1', 'Y1', 'X2', 'Y2', 'X3', 'Y3', 'X4', 'Y4'];
	squaresTotal = 60;
	squaresLevels = 12;
	squares = [];
	rotate = 0;
	point1 = {x: -1, y: -1};
	point2 = {x: -1, y: -1};
	point3 = {x: -1, y: -1};
	point4 = {x: -1, y: -1};
	squaresHoverRadiusAdjust = 2;
	squaresLevelColors = this._genColors.array.rgba(
		'#FF0000',
		'#494949',
		this.squaresLevels, 1, 0.4
	);

  constructor() {
		super(...arguments);
		for (let i = 0; i < this.squaresTotal; i++) {
			this.squares.push({
				x: -1,
				y: -1,
				angle: 0
			});
		}
  }
  
  keyDownEvent(e) {
		const index = e.keyCode - 49; // 1 through 8 on keyboard
		if (index > -1 && index < this.rotations.length) {
			this.rotations[index] = !this.rotations[index];
		}
	}

	draw(ctx, state) {
		ctx.clearRect(0, 0, state.w, state.h);

		return;
		let size = (state.w < state.h ? state.w : state.h) * 0.1;
		let gradient = ctx.createRadialGradient(state.xCenter, state.yCenter, 20, state.xCenter, state.yCenter, size);
		gradient.addColorStop(0, 'rgba(0,0,100,1)');
		gradient.addColorStop(1, 'rgba(0,0,100,0)');
		ctx.beginPath();
		ctx.arc(state.xCenter, state.yCenter, size, 0, 2 * Math.PI);
		ctx.fillStyle = gradient;
		ctx.fill();
		ctx.closePath();

		this.rotate += 0.1;
		for (let lvl = 1; lvl < this.squaresLevels + 1; lvl++ ) {
			ctx.strokeStyle = lvl === this.squaresLevels ? 'rgba(0,0,0,0.2)' : this.squaresLevelColors[lvl];
			const growth = Math.pow(lvl * 1.5, 2);
			for (let squareNum = 0; squareNum < this.squaresTotal; squareNum++) {
				const square = this.squares[squareNum];
				square.angle += state.mouseDistanceFromCenter / 500000;
				const squareAngle = square.angle * (lvl % 2 === 0 ? -1 : 1);
				this.point1.x = state.xCenter + Math.cos(squareNum + (squareAngle * (this.rotations[0] ? this.squaresHoverRadiusAdjust : 1) + (1 / 2 * Math.PI) )) * growth;
				this.point1.y = state.yCenter + Math.sin(squareNum + (squareAngle * (this.rotations[1] ? this.squaresHoverRadiusAdjust : 1) + (1 / 2 * Math.PI) )) * growth;
				this.point2.x = state.xCenter + Math.cos(squareNum + (squareAngle * (this.rotations[2] ? this.squaresHoverRadiusAdjust : 1) + (2 / 2 * Math.PI) )) * growth;
				this.point2.y = state.yCenter + Math.sin(squareNum + (squareAngle * (this.rotations[3] ? this.squaresHoverRadiusAdjust : 1) + (2 / 2 * Math.PI) )) * growth;
				this.point3.x = state.xCenter + Math.cos(squareNum + (squareAngle * (this.rotations[4] ? this.squaresHoverRadiusAdjust : 1) + (3 / 2 * Math.PI) )) * growth;
				this.point3.y = state.yCenter + Math.sin(squareNum + (squareAngle * (this.rotations[5] ? this.squaresHoverRadiusAdjust : 1) + (3 / 2 * Math.PI) )) * growth;
				this.point4.x = state.xCenter + Math.cos(squareNum + (squareAngle * (this.rotations[6] ? this.squaresHoverRadiusAdjust : 1) + (4 / 2 * Math.PI) )) * growth;
				this.point4.y = state.yCenter + Math.sin(squareNum + (squareAngle * (this.rotations[7] ? this.squaresHoverRadiusAdjust : 1) + (4 / 2 * Math.PI) )) * growth;

				ctx.beginPath();
				ctx.moveTo(this.point1.x, this.point1.y);
				ctx.lineTo(this.point2.x, this.point2.y);
				ctx.lineTo(this.point3.x, this.point3.y);
				ctx.lineTo(this.point4.x, this.point4.y);
				ctx.lineTo(this.point1.x, this.point1.y);
				ctx.stroke();
				ctx.closePath();
			}
		}
	}
}
