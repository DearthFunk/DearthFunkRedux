import Animation from './animation';

export default class Tracer extends Animation {
	tracerMaxLength = 30;
	tracerHoverPoints = [];
	tracerColors = [];
	tracerMaxPointsPerLine = 200;
	tracerCircleAddSpacing = 0;
	mouseDown = false;

	mouseMoveEvent(state) {
		if (this.tracerHoverPoints.length >= this.tracerMaxLength) {
			this.tracerHoverPoints.splice(0, 1);
		}
		for (let i = 1; i < 2; i++) {
			this.tracerHoverPoints.push({
				x: state.x,
				y: state.y - 105,
				p: []
			});
		}
		this.tracerColors = this._genColors.array.rgba('#FF0000', '#0000FF', this.tracerHoverPoints.length, 1, 1);
	}

	draw(ctx, state) {
		return;
		if (this.mouseDown !== state.mouseDown) {
			this.tracerHoverPoints = [];
			this.mouseDown = state.mouseDown;
		}
		if (state.mouseDown) {
			this.mouseMoveEvent(state);
		}

		ctx.clearRect(0, 0, state.w, state.h);
		const spacing = state.h / 40;
		let spacingAdjust = state.mouseDistanceFromCenter / state.w * 2;
		this.tracerCircleAddSpacing += state.mouseDown ? -spacingAdjust : spacingAdjust;
		if (this.tracerCircleAddSpacing > spacing) {this.tracerCircleAddSpacing = 0; }
		if (this.tracerCircleAddSpacing < 0) {this.tracerCircleAddSpacing = spacing; }
		const numCircles = Math.floor(state.h / spacing);

		for (let i = 0; i < numCircles; i++) {
			ctx.beginPath();
			ctx.arc(state.xCenter, state.yCenter, i * spacing + this.tracerCircleAddSpacing, 0, Math.PI * 2, false);
			ctx.strokeStyle = '#000000'; //this._genColors.convert.rgba('#FFFF00', 1 - (i * spacing) / (state.h/2));
			ctx.stroke();
			ctx.closePath();
		}

		if (this.tracerHoverPoints.length > 0) {
			for (let i = 0; i <  this.tracerHoverPoints.length - 1; i++) {
				const point = this.tracerHoverPoints[i];
				const point2 = this.tracerHoverPoints[i + 1];
				if (state.mouseDown) {
					ctx.beginPath();
					ctx.moveTo(point.x, point.y);
					ctx.lineTo(point2.x, point2.y);
					ctx.strokeStyle = this.tracerColors[i];
					ctx.stroke();
					ctx.closePath();
				}

				if (point.p.length < this.tracerMaxPointsPerLine && state.mouseDown) {
					point.p.push({
						r: this._genColors.generate.randomNumber(3, 5, 3),
						x: point.x,
						y: point.y,
						xD: this._genColors.generate.randomNumber(-2, 2, 3),
						yD: this._genColors.generate.randomNumber(-2, 2, 3),
						rD: this._genColors.generate.randomNumber(0.1, 0.4, 2)
					});
				}

				for (let a = 0; a < point.p.length; a++) {
					const dot = point.p[a];
					dot.x += dot.xD;
					dot.y += dot.yD;
					dot.r -= dot.rD;
					if (dot.r < 0) {
						point.p.splice(a, 1);
					} else {
						ctx.beginPath();
						ctx.arc(dot.x, dot.y, dot.r, 0, Math.PI * 2, false);
						ctx.fillStyle = '#FFFFFF';
						ctx.fill();
						ctx.strokeStyle = this.tracerColors[i];
						ctx.stroke();
						ctx.closePath();
					}
				}
			}
		}
	}
}
