import Animation from './animation';

export default class Stopper extends Animation {
	stopperLines = [];
	stopperSizeMin = 20;
	stopperSizeMax = 100;
	stopperTotal = 500;
	stopperSize = this.stopperSizeMin;
	mouseDown = false;

	constructor(state) {
		super(...arguments);
		for (let i = 0; i < this.stopperTotal; i ++) {
			const length = Math.random();
			this.stopperLines.push({
				x: state.xCenter,
				y: state.yCenter,
				c: this._genColors.random.rgba(0, false, 1 - length),
				angle: Math.random() * Math.PI * 2,
				speed: this._genColors.generate.randomNumber(0.01, 0.015, 3),
				length: length,
				hitCircle: false
			});
		}
	}

	draw(ctx, state) {
		return;
		if (this.mouseDown !== state.mouseDown) {
			this.mouseDown = state.mouseDown;
			if (this.stopperSize < this.stopperSizeMax) {
				this.stopperSize += 20;
			} else {
				this.stopperSize += (this.stopperSizeMax - this.stopperSize) / 2;
			}
		}
		ctx.clearRect(0, 0, state.w, state.h);

		if (this.stopperSize > this.stopperSizeMin) {
			this.stopperSize -= 0.6;
		}
		if (this.stopperSize < 0) {
			this.stopperSize = 0;
		}
		const radAdjust = state.w / 3 * 0.8;
		for (let i = 0; i < this.stopperLines.length; i++) {
			const line = this.stopperLines[i];
			const x = state.xCenter + Math.cos(line.angle) * line.length * radAdjust;
			const y = state.yCenter + Math.sin(line.angle) * line.length * radAdjust;

			const d = Math.sqrt(Math.pow(state.x - x, 2) + Math.pow(state.y - y, 2));
			if ( d > this.stopperSize + 2) {
				line.angle += line.speed;
			}

			ctx.beginPath();
			ctx.moveTo(state.xCenter, state.yCenter);
			ctx.lineTo(x, y);
			ctx.strokeStyle = line.c;
			ctx.stroke();
			ctx.closePath();

			ctx.beginPath();
			ctx.fillStyle = '#FFFF00';
			ctx.arc(x, y, 1.2, 0, Math.PI * 2, false);
			ctx.fill();
			ctx.closePath();
		}
		if (state.mouseDistanceFromCenter < radAdjust){
			ctx.beginPath();
			ctx.arc(state.x, state.y, this.stopperSize, 0, Math.PI * 2, false);
			ctx.strokeStyle = '#FFFFFF';
			ctx.fillStyle = 'rgba(0,0,0,0.4)';
			ctx.stroke();
			ctx.fill();
			ctx.closePath();
		}
	}
}
