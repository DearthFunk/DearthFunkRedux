import Animation from './animation';

class Whirly {
	constructor(
		x = 0,
		y = 0,
		baseColor = 'rgba(255,255,0,0.5)',
		baseSize = 7,
		drawLines = false,
		total = 75,
		maxSize = 60,
		particles = []
	) {
		this.total = total;
		this.maxSize = maxSize;
		this.baseColor = baseColor;
		this.baseSize = baseSize;
		this.drawLines = drawLines;
		this.particles = particles;
		this.x = x;
		this.y = y;
	}
}

class Particle {
	constructor(x, y, genColors) {
		this.x = x;
		this.y = y;
		this.size = genColors.generate.randomNumber(0.01, 1, 2);
		this.fillColor = genColors.random.rgba();
		this.xMod = genColors.generate.randomNumber(-2, 2, 4) / 10;
		this.yMod = genColors.generate.randomNumber(-2, 2, 4) / 10;
		this.angle = 0;
		this.speed = genColors.generate.randomNumber(0.001, 0.1, 4); // more like spin rate
		this.orbit = Math.random()
	}

	_tick(state, index, genColors) {
		let mdist = genColors.getDistance(this.x, this.y, state.x, state.y) / 2;
		this.x += this.xMod;
		this.y += this.yMod;
		this.angle += this.speed;
		const orbit = (state.xCenter * this.orbit) / (mdist);
		this.x += Math.cos(index + this.angle) * orbit;
		this.y += Math.sin(index + this.angle) * orbit;
		this.size -= (0.02 * (mdist / 200));
	}
	
	_shouldKill(w, h) {
		return this.x < 0 || this.x > w || this.y < 0 || this.y > h || this.size <= 0; 
	}

	draw(ctx, maxSize, baseSize, baseColor, whirlyX, whirlyY, drawLines) {
		ctx.beginPath();
		const r = this.size * maxSize / 2;
		ctx.fillStyle = r < baseSize ? baseColor : this.fillColor;
		//console.log(baseColor);
		ctx.arc(this.x, this.y, r, 0, Math.PI * 2, true);
		ctx.fill();
		ctx.closePath();
		//faded line
		if (drawLines) {
			ctx.beginPath();
			ctx.strokeStyle = 'rgba(255,0,0,0.4)';
			ctx.moveTo(whirlyX, whirlyY);
			ctx.lineTo(this.x, this.y);
			ctx.stroke();
			ctx.closePath();
		}
	}
}
export default class WhirlyParticles extends Animation{

	whirles = [
		new Whirly(200, 200),
		new Whirly(200, 600),
		new Whirly(600, 200),
		new Whirly(600, 600),
		new Whirly(400, 400, 'rgba(0,0,0,1)', 10, true)
	]

	draw(ctx, state) {
		ctx.globalCompositeOperation = 'source-over';
		ctx.clearRect(0, 0, state.w, state.h);		

		this.whirles.forEach((whirly) => {
			for (let i = 0; i < whirly.total; i++) {
				//should not need this
				if (whirly.particles[i] == null) {
					whirly.particles.push(
						new Particle(whirly.x, whirly.y, this._genColors)
					);
				}

				let particle = whirly.particles[i];
				particle._tick(state, i, this._genColors);
				if (particle._shouldKill(state.w, state.h)) {
					particle = new Particle(whirly.x, whirly.y, this._genColors);
					whirly.particles[i] = particle;
				}
				particle.draw(ctx, whirly.maxSize, whirly.baseSize, whirly.baseColor, whirly.x, whirly.y, whirly.drawLines);
			}
		})

	}
}
