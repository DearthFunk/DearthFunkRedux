import Animation from './animation';

class BubblesThemeSet {
	globalCompositeOperation = 'source-over';

	constructor(genColors) {
		this._genColors = genColors;
		this.index = 0;
		this.themes = [
			['#FF0000', '#000000', '#BBBBFF'],
			['#0074D9', '#001f3f', '#F012BE'],
			['#85144b', '#39CCCC', '#7FDBFF'],
		]
		this.theme = this.themes[this.index];
	}
	
	next() {
		this.index++;
		this.theme = this.themes[this.index] || this.randomTheme();
		return this.theme;
	}

	randomTheme() {
		let rndmHex1 = this._genColors.random.hex();
		let rndmHex2 = this._genColors.random.hex();
		return this._genColors.array.hex(rndmHex1, rndmHex2, 3);
	}

	get color() {
		let level1 = Math.random() < 0.8;
		let level2 = Math.random() < 0.9;
		return level1 ?
			this._genColors.convert.rgba(
				this.theme[0],
				this._genColors.generate.randomNumber(0, 0.6, 2)
			) :
			level2 ?
				this._genColors.convert.rgba(
					this.theme[1],
					this._genColors.generate.randomNumber(0, 0.5, 2)
				) :
				this.theme[2];
	}
}

export default class Bubbles extends Animation {

	shrinkAtMouseDistance = true;
	bubbleMouseDecay = 0.2;
	centerMouseBubble = 20;
	mouseDown = false;

	themes = new BubblesThemeSet(this._genColors);
	bubbles = new Array(1000).fill(this.newBubble());

	/////////////////////////////////////

	newBubble(distanceFromCenter = this.centerMouseBubble) {
		distanceFromCenter = Math.min(distanceFromCenter, this.centerMouseBubble);
		return {
			position: { x: 0, y: 0 },
			size: this._genColors.generate.randomNumber(0.01, distanceFromCenter, 3),
			fillColor: this.themes.color,
			hasConnector: Math.random() < 0.2,
			xMod: this._genColors.generate.randomNumber(-2, 2, 3),
			yMod: this._genColors.generate.randomNumber(-2, 2, 3),
			speed: this._genColors.generate.roundedNumber(this._genColors.generate.randomNumber(0, 1, 2) / 50, 8),
			orbit: Math.random() * 3,
			angle: 0
		}
	}

	_handleMouse(mouseDown) {
		if (this.mouseDown !== mouseDown) {
			this.themes.next();
		}
		this.mouseDown = mouseDown;
	}

	draw(ctx, state) {
		var oldArray = ctx.getImageData(0,0,state.w,state.h); //todo: make this re-useable
		for(var d=3;d<oldArray.data.length;d+=4){ //count through only the alpha pixels

			//dim it with some feedback, I'm using .9
			oldArray.data[d] = Math.floor(oldArray.data[d]*.7);
		}
		ctx.putImageData(oldArray,0,0);
		//ctx.clearRect(0, 0, state.w, state.h);

		ctx.globalCompositeOperation = 'lighter';
		ctx.lineWidth = 1;
		this._handleMouse(state.mouseDown);
		for (let i = 0; i < this.bubbles.length; i++) {
			const bubble = this.bubbles[i];
			bubble.size -= 100 / state.mouseDistanceFromCenter;
			bubble.angle += bubble.speed;

			bubble.position.x += Math.cos(i + bubble.angle) * bubble.orbit * (state.mouseDistanceFromCenter / 500);
			bubble.position.y += Math.sin(i + bubble.angle) * bubble.orbit * (state.mouseDistanceFromCenter / 500);

			let x = state.xCenter + bubble.position.x;
			let y = state.yCenter + bubble.position.y;

			if (this.shrinkAtMouseDistance) {
				let distance = this._genColors.getDistance(x, y, state.xCenter, state.yCenter);
				if (distance > state.mouseDistanceFromCenter) {
					bubble.size -= this.bubbleMouseDecay;
				}
			}
			if (bubble.size < 0) { this.bubbles[i] = this.newBubble(state.mouseDistanceFromCenter); }
			if (bubble.size > 0) {
				
				//draw line
				if (bubble.hasConnector) {
					ctx.beginPath();
					ctx.moveTo(state.xCenter, state.yCenter);
					let x = state.xCenter + bubble.position.x;
					let y = state.yCenter + bubble.position.y;
					ctx.lineTo(x, y);
					var grd = ctx.createLinearGradient(state.xCenter, state.yCenter, x, y);
					grd.addColorStop(0,   'rgba(255, 255, 255, 0)');
					grd.addColorStop(0.33, 'rgba(255, 255, 255, 0)');
					grd.addColorStop(0.66,'rgba(255, 255, 255, 1)');
					grd.addColorStop(1, this._genColors.convert.rgba(bubble.fillColor, 1));
					ctx.strokeStyle = grd;
					ctx.stroke();
					ctx.closePath();
				}

				//draw circle
				ctx.beginPath();
				ctx.fillStyle = bubble.fillColor;
				ctx.arc(x, y, bubble.size, 0, Math.PI * 2, true);
				ctx.fill();
				ctx.closePath();
			}
		}
	}
}
