import Animation from './animation';

class Galaxy {
	constructor(numberOfStars, lineColor, dotColor, speed, orbitFlux, isReversed, isSolidLine, isMagnetic, speedUpOnMouseDown) {
		return {
			stars: this._createStars(numberOfStars, speed, orbitFlux),
			speed, 
			orbitFlux,
			lineColor,
			dotColor,
			isSolidLine,
			isReversed,
			isMagnetic,
			speedUpOnMouseDown
		}
	}

	_createStars(numberOfStars, speed, orbitFlux) {
		let arr = [];
		for (let i = 0; i < numberOfStars; i++) {
			arr.push(this._createStar(speed, orbitFlux));
		}
		return arr;
	}

	_createStar(speed, orbit) {
		return {
			x: Math.random(),
			y: Math.random(),
			xD: 0,
			yD: 0,
			size: 1 + Math.random(),
			angle: 0,
			speed: speed * 0.004,
			orbit: Math.random() * 150 * orbit
		}
	}
}

export default class LineConnections extends Animation  {
	globalCompositeOperation = 'source-over';
	cursorAffectDistance = 15;
	lineFlux = 200;
	containerHeight = 50;
	sidePadding = 240;
	twoPI = Math.PI * 2;

	galaxies = [
		//amount, linecolor, dotcolor, speed, orbit, isReversed, isSolidLine, isMagnetic
		new Galaxy(400, '#666666', '#000000', 0.2,   2, true, false, false, false),
		new Galaxy(200, '#FF0000', 'rgba(255, 0, 60, 0.3)', 1, 1, false, false, true, 0.005),
		new Galaxy(150, '#000000', '#FFFF00', 0.8, 0.5, true, true, false, 0.01),
	];
	
	draw(ctx, state) {
		//todo: the bubble grow effect when the mouse is near a star doesn't fade in/out correctly
		if (!state.w || !state.h) { return; } //todo: this shuold not get run until w/h has been set at least once
		ctx.clearRect(0, 0, state.w, state.h);

		let galaxy; //function should keep to the 'animation flow' from here on

		galaxy = this.galaxies[0];
		this._updateStars(galaxy, state);
		this._drawLines(ctx, galaxy);
		
		galaxy = this.galaxies[1];
		this._updateStars(galaxy, state);
		this._drawLines(ctx, galaxy);
		this._drawDots(ctx, galaxy);		
		
		galaxy = this.galaxies[2];
		this._updateStars(galaxy, state);
		this._drawLines(ctx, galaxy);
		if (state.mouseDown) {

			this._drawDots(ctx, galaxy);
		}

		this._drawDebugString(ctx, state);
	}

	_drawLines(ctx, galaxy) {
		// draw lines
		const newArray = galaxy.stars.sort(function (a, b) {
			return a.x - b.x;
		});

		//todo: this is shit code, maybe use some version of sort
		for (let a = 0; a < newArray.length; a++) {
			const p1 = newArray[a];
			for (let b = 0; b < 10; b++) {
				if (a + b >= newArray.length) { break; }
				const p2 = newArray[a + b];

				const d = Math.sqrt( Math.pow(p1.xD - p2.xD, 2) + Math.pow(p1.yD - p2.yD, 2) );
				if (d < this.lineFlux) {
					ctx.beginPath();
					if (galaxy.isSolidLine) {
						ctx.strokeStyle = galaxy.lineColor;
					}
					else {
						let opacity = this._genColors.generate.roundedNumber(d/this.lineFlux/2, 2);
						ctx.strokeStyle = this._genColors.convert.rgba(galaxy.lineColor, opacity);
					}
					ctx.lineWidth = 1 - (d / this.lineFlux);
					ctx.moveTo(p1.xD, p1.yD);
					ctx.lineTo(p2.xD, p2.yD);
					ctx.stroke();
					ctx.closePath();
				}
			}
		}
	}

	_drawDots(ctx, galaxy) {
		
		for (let i = 0; i < galaxy.stars.length; i++) {
			let star = galaxy.stars[i];
			
			let size = star.size * star._adjustment;
			let bubbleBust = 1;
			if (galaxy.isSolidLine) {
				bubbleBust = 5;
				var gradient = ctx.createRadialGradient(star.xD, star.yD, size, star.xD, star.yD, size * bubbleBust);
				let color = this._genColors.convert.rgba(galaxy.dotColor, 0.1);
				gradient.addColorStop(0,   'rgba(255, 255, 255, 1)');
				gradient.addColorStop(0.3, color);
				gradient.addColorStop(0.5, 'rgba(255, 255, 255, 0.6)');				
				gradient.addColorStop(1,   'rgba(255, 255, 255, 0)');				
				ctx.fillStyle = gradient;		
			}
			else {
				ctx.fillStyle =  galaxy.dotColor;
			}
			ctx.beginPath();
			ctx.arc(star.xD, star.yD, size * bubbleBust, 0, this.twoPI, true);
			ctx.fill();
			ctx.closePath();
		}
	}

	_getMagneticMultiplier(star, state) {
		let mouseDistanceFromStar = this._genColors.getDistance(star.xD, star.yD, state.x, state.y);
		let inMouseRange = mouseDistanceFromStar < this.cursorAffectDistance;
		if (!inMouseRange) { return 1;}
		return Math.min(this.cursorAffectDistance - mouseDistanceFromStar);
	}

	_updateStars(galaxy, state) {
		const w = state.w - (this.sidePadding * 2);
		const h = this.containerHeight;

		for (let i = 0; i < galaxy.stars.length; i++) {
			let star = galaxy.stars[i];
			star._adjustment = galaxy.isMagnetic ? this._getMagneticMultiplier(star, state) : 1;
			
			star.angle += star.speed * star._adjustment;
			if (galaxy.speedUpOnMouseDown && state.mouseDown) {
				star.angle += galaxy.speedUpOnMouseDown;
			}
			let starAngle = star.angle;
			if (galaxy.isReversed) {
				starAngle *= -1;
			}
			
			star.xD = star.x * w + ( Math.cos(i + starAngle) * star.orbit) + this.sidePadding;
			star.yD = star.y * h + ( Math.sin(i + starAngle) * star.orbit) + (state.yCenter - (h / 2));
		}
	}

	_drawDebugString(ctx, state) {
		//todo: make it glitch out
		ctx.save();
		ctx.fillStyle = '#444';
		let leftPadd = state.leftNavWidth + 50;
		ctx.fillText(state.debugString, leftPadd, state.yCenter - (this.containerHeight*2));
		ctx.translate(state.w -leftPadd, state.yCenter);  
		ctx.rotate(Math.PI);  
		ctx.fillText(state.debugString, 0, 12 - (this.containerHeight*2)	); //todo should be minus line height
		ctx.restore();		
	}
}
