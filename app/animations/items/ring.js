import Animation from './animation';

class Cluster {
	constructor(totalDots, radius, fillStyle, strokeStyle, angle, genColors, reversed) {
		this.radius = radius;
		this.fillStyle = fillStyle;
		this.strokeStyle = strokeStyle;
		this.angle = angle;
		this.reversed = reversed;
		this.x = 0;
		this.y = 0;

		this.dots = Array.from(Array(totalDots), (x, index) => {
			return new Dot(
				index,
				this.fillStyle,
				genColors
			);
		});
	}
}
class Dot {
	constructor(index, fillStyle, genColors) {
		this.genColors = genColors;
		this.index = index; //todo: smell + dup on reset
		this.fillStyle = fillStyle;
		this.radius = genColors.generate.randomNumber(5, 20, 3);
		this.orbit = Math.random() * 10;
		this.angle = 0;
		this.setSpeed();
		this.setRadius();
	}

	setSpeed() {
		this.speed = this.genColors.generate.randomNumber(0, 0.04, 2);
	}
	setRadius() {
		this.radius = this.genColors.generate.randomNumber(5, 20, 3);
	}

	reset() {
		this.setSpeed();
		this.setRadius();
		this.orbit = Math.random() * 10;
		this.angle = 0;
	}

	draw(ctx, clusterX, clusterY) {
		ctx.fillStyle = this.fillStyle;
		ctx.beginPath();
		ctx.arc(
			clusterX + Math.cos(this.index + this.angle) * this.orbit * this.angle * 4.5,
			clusterY + Math.sin(this.index + this.angle) * this.orbit * this.angle * 4.5,
			this.radius,
			0, Math.PI * 2, true
		);
		ctx.fill();
		ctx.stroke();
		ctx.closePath();
	}
}

export default class Ring extends Animation{
	mainRadius = 240;
	totalClusters = 20;
	totalDotsPerCluster = 40;
	ringClusterAngle = 0; //todo: smell
	image = new Image();
	tempCanvas;
	tempCtx;

	constructor() {
		super(...arguments);
		this.image.src = 'dearthfunkdark.png';
		this.tempCanvas = document.createElement('canvas');
		this.tempCtx = this.tempCanvas.getContext('2d');

		this.clusters = Array.from(Array(this.totalClusters), (x, index) => {
			return new Cluster(
				this.totalDotsPerCluster,
				this.mainRadius,
				'#FFFFFF',
				'#FF0000',
				index/this.totalClusters,
				this._genColors,
				index % 2 === 0
			);
		})
	}

	drawCluster(ctx, state, cluster) {
		ctx.fillStyle = cluster.fillStyle;
		ctx.strokeStyle = cluster.strokeStyle;
		const clusterA = cluster.angle * 2 * Math.PI;
		const clusterAdjust = cluster.reversed ? this.ringClusterAngle : -1 * this.ringClusterAngle;

		cluster.x = cluster.radius * Math.cos(clusterA + clusterAdjust) + state.xCenter;
		cluster.y = cluster.radius * Math.sin(clusterA + clusterAdjust) + state.yCenter;
		cluster.dots.forEach((dot) => {
			dot.radius -= 5 / state.mouseDistanceFromCenter;
			dot.angle += dot.speed;
			if (dot.radius < 0) {
				let index = dot.index;
				dot.reset(this._genColors);
			}
			else {
				dot.draw(ctx, cluster.x, cluster.y);
			}
		});
	}

	draw(ctx, state) {
		ctx.globalCompositeOperation = 'lighter';
		ctx.lineWidth = 1;
		ctx.clearRect(0, 0, state.w, state.h);
		this.ringClusterAngle += state.mouseDistanceFromCenter / 40000;

		this.clusters.forEach((item) => {
			this.drawCluster(ctx, state, item);
		});
		this.drawMouseOverEffect(ctx, state);
	}

	getBackgroundFillStyle(ctx, state) {
		//todo: this is wastefull, watch state changes then draw and save the fillStyle, intead of drawing/createPattern over and over and over over and over
		this.tempCanvas.width = state.w;
		this.tempCanvas.height = state.h;
		this.tempCtx.drawImage(this.image,
			state.xCenter - (this.image.width/2),
			state.yCenter - (this.image.height/2)
		);
		return ctx.createPattern(this.tempCanvas, 'no-repeat');
	}
	
	drawMouseOverEffect(ctx, state) {
		ctx.globalCompositeOperation = 'source-atop';
		ctx.fillStyle = this.getBackgroundFillStyle(ctx, state);
		ctx.beginPath();
		ctx.arc(
			state.x,
			state.y,
			100,
			0, Math.PI * 2, true
		);
		ctx.fill();

		ctx.closePath();
	}
}
