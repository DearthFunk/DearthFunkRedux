import genColors from '../gen-colors';

export default class Animation {

	_genColors = genColors;

	constructor(_id, name, description) {
		this._id = _id;
		this.name = name || _id;
		this.description = description || '';
	}
}
