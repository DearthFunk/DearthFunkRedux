import { tracked } from '@glimmer/tracking';

export class CanvasState {
  x = 0; //mouse x
  y = 0; //mouse y
  @tracked w = 0;
  @tracked h = 0;
  @tracked leftNavWidth = 200; //todo: tie to css style of left nav
  @tracked mouseDown = false;
  @tracked adjustLeftNav = true;

  get xCenter() {
    return this.adjustLeftNav ?
    Math.floor((this.w - this.leftNavWidth) / 2) + this.leftNavWidth:
    Math.floor(this.w / 2);
  }

  get yCenter() {
    return Math.floor(this.h / 2);
  }

  get mouseDistanceFromCenter() {
    return Math.floor(Math.sqrt(
      Math.pow(this.x - this.xCenter, 2) +
      Math.pow(this.y - this.yCenter, 2)
    ));
  }

  shuffleWord(word) {
    var shuffledWord = '';
    word = word.split('');
    while (word.length > 0) {
      shuffledWord +=  word.splice(word.length * Math.random() << 0, 1);
    }
    return shuffledWord;
  }

  //todo: put in its own reusable module
  counter = 1;
  debugOn = false;
  stepAmnts = [200, 40]; //off on
  get debugString() {
    let string = `0x-${this.counter}-${this.w}x${this.h}|${this.x}*${this.y}|${this.xCenter}x${this.yCenter}|${this.mouseDown ? 1: 0}·${this.mouseDistanceFromCenter}`;
    this.counter++;

    let step = this.stepAmnts[+ this.debugOn]
    if (this.counter % step === 0){
      this.debugOn = !this.debugOn;
    }
    return this.debugOn ? this.shuffleWord(string, this.counter) : string;
  }
}